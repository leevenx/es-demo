### elasticsearch Java API练习项目
####使用说明
 - 基于spring boot 2.1.7.RELEASE 版本开发
 - 依赖spring-boot-starter-data-elasticsearch，自动创建ES索引
 - ES java api 版本要与 ES安装的版本一致，否则会出现各种意想不到的问题！
 - es 版本：6.2.2
 - 需要安装ik分词器