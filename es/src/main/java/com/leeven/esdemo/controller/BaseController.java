package com.leeven.esdemo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.leeven.esdemo.entity.Shop;
import com.leeven.esdemo.repository.ShopRepository;
import com.leeven.esdemo.vo.Result;
import com.leeven.esdemo.vo.ShopVO;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.GeoDistanceSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * @author leeven
 * @date 2019-08-10
 */
@RestController
@RequestMapping("/")
public class BaseController {

    @Autowired
    private ShopRepository shopRepository;

    private Client client = new PreBuiltTransportClient(Settings.EMPTY)
            .addTransportAddress(new TransportAddress(new InetSocketAddress("localhost", 9300)));

    @RequestMapping("/sort")
    public List<ShopVO> orderByTime(@RequestParam(defaultValue = "0") int pageNo, @RequestParam(defaultValue = "10") int pageSize) {
        SearchRequestBuilder srb = client.prepareSearch("shop");
        srb.addSort(SortBuilders.fieldSort("createTime").order(SortOrder.DESC));
        srb.setFrom(pageNo * pageSize).setSize(pageSize);

        SearchResponse searchResponse = srb.execute().actionGet();

        return convertShopVOs(searchResponse);
    }

    @RequestMapping("/distance")
    public Result getByDistance(@RequestParam(defaultValue = "0") int pageNo, @RequestParam(defaultValue = "10") int pageSize) {
        Result result = new Result();
        double lat = 41.12345;
        double lon = 71.12345;
        try {
            SearchRequestBuilder srb = client.prepareSearch("shop");

            GeoDistanceSortBuilder distanceSortBuilder = SortBuilders.geoDistanceSort("location", lat, lon)
                    .order(SortOrder.ASC).unit(DistanceUnit.KILOMETERS);

            srb.addSort(distanceSortBuilder);
            srb.setFrom(pageNo * pageSize).setSize(pageSize);

            System.out.println(srb.toString());

            SearchResponse response = srb.execute().actionGet();
            List<ShopVO> list = convertShopVOs(response);
            result.setData(list);
            result.setStatus(200);
        } catch (Exception e) {
            result.setMsg("发生异常");
            result.setStatus(500);
            e.printStackTrace();
        }
        return result;
    }

    private List<ShopVO> convertShopVOs(SearchResponse response) {
        SearchHit[] hits = response.getHits().getHits();
        ObjectMapper om = new ObjectMapper();
        List<ShopVO> list = new ArrayList<>();
        for (SearchHit hit : hits) {
            Map<String, Object> sourceMap = hit.getSourceAsMap();
            ShopVO shopVO = om.convertValue(sourceMap, ShopVO.class);

            double distance = 0d;
            Object[] sortValues = hit.getSortValues();
            for (Object sortValue : sortValues) {
                if (sortValue instanceof Double) {
                    distance = (double) sortValue;
                    if (Double.isFinite(distance)) {
                        BigDecimal distanceBig = new BigDecimal(distance).setScale(3, BigDecimal.ROUND_HALF_DOWN);

                        shopVO.setDistance(distanceBig.doubleValue());
                        break;
                    }
                }
            }
            list.add(shopVO);
        }
        return list;
    }

    @RequestMapping("/shops")
    public Result shops() {
        Result result = new Result();
        try {
            List<ShopVO> shopVOs = new ArrayList<>();
            Iterable<Shop> shops = shopRepository.findAll();
            for (Shop shop : shops) {
                ShopVO shopVO = new ShopVO();
                BeanUtils.copyProperties(shop, shopVO);
                shopVOs.add(shopVO);
            }
            result.setData(shopVOs);
            result.setStatus(200);
        } catch (Exception e) {
            result.setMsg("发生异常");
            result.setStatus(500);
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping("/initData")
    public Result initData() {
        double lat = 41.12345;
        double lon = 71.12345;

        LocalDateTime ldt = LocalDateTime.now();
        Random r = new Random();
        for (int i = 0; i < 100; i++) {
            double max = 0.0001;
            double min = 0.00001;
            Random random = new Random();
            double s = random.nextDouble() % (max - min + 0.01) + max;
            DecimalFormat df = new DecimalFormat("######0.00000");
            String lons = df.format(s + lon);
            String lats = df.format(s + lat);

            Shop shop = new Shop();
            shop.setId((long) (i + 1));
            shop.setName("名称" + i + 1);
            shop.setDescription("描述！！！" + i);
            shop.setLocation(lats + "," + lons);

            int d = r.nextInt(30);
            int h = r.nextInt(12);
            LocalDateTime ldt2 = ldt.plusDays(d).plusHours(h);

            shop.setCreateTime(localDateTime2Date(ldt2));
            shopRepository.save(shop);
        }
        return new Result(200, "初始化数据成功！");
    }

    private Date localDateTime2Date(LocalDateTime ldt) {
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime zdt = ldt.atZone(zoneId);
        return Date.from(zdt.toInstant());
    }
}
