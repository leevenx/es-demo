package com.leeven.esdemo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;

import java.util.Date;

@Document(indexName = "shop")
@Data
public class Shop {
    @Id
    Long id;

    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    String name;

    /**
     * 经纬度，（维度在前，经度在后，如：41.12523,71.12523）
     */
    @GeoPointField
    String location;

    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    String description;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:sss")
    @Field(type = FieldType.Date, pattern = "yyyy-MM-dd HH:mm:sss", format = DateFormat.custom)
    Date createTime;

}
