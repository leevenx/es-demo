package com.leeven.esdemo.repository;

import com.leeven.esdemo.entity.Shop;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ShopRepository extends ElasticsearchRepository<Shop, Long> {
}
