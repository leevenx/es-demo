package com.leeven.esdemo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class ShopVO {
    Long id;

    String name;

    String location;

    String description;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:sss")
    Date createTime;

    Double distance;
}
